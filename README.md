# Roman Numerals Calculator
Calculator which has its inputs and output in Roman numerals.
Try out the demo [here](https://roman-numerals-calculator.netlify.com/)!

### Limitations

Calculations may only be performed for numbers ranging from 1 to 3999.
 
## Technologies
The calculator is built in ReactJS v16 with Create React App, and managed with Yarn. The following main libraries are used:

 - Node Sass
 - [React TextFit](https://github.com/malte-wessel/react-textfit)
 - Enzyme
 - Jest Enzyme
 
## Prerequisites

 To install the app locally, your development machine will need to meet the following conditions:
 - Node 8.16.0 or Node 10.16.0 or a later version
 - Yarn (latest version)

## Installation

To run this app locally, run the following commands in order:
 - `git clone https://dkelsen@bitbucket.org/dkelsen/roman-numerals-calculator.git`
 - `cd roman-numerals-calculator`
 - `yarn add lockfile`
 - `yarn start`

This runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Testing

To run all tests, run the command `yarn test`.

## Deployment
The calculator is deployed on Netlify.

## Acknowledgments
The following UI design was implemented for this project:

 - [Daily UI 004 - Calculator UI Design](https://dribbble.com/shots/2858221-Daily-UI-004-Calculator-UI-Design)
