export const findByTestAttribute = (component, attribute) => component.find(`[data-test='${attribute}']`);

const getArabicValue = (romanNumeral) => {
  const values = {
    'I': 1,
    'V': 5,
    'X': 10,
    'L': 50,
    'C': 100,
    'D': 500,
    'M': 1000,
  }
  return values[romanNumeral];
}

export const convertRomanToArabic = (romanNumerals) => {
  let largestRomNum = 'I';
  let totalArabicValue = 0;
  const romNumArray = romanNumerals.split('').reverse();

  romNumArray.forEach(romNum => {
    if (getArabicValue(romNum) >= getArabicValue(largestRomNum)) {
      totalArabicValue += getArabicValue(romNum);
      largestRomNum = romNum;
    } else {
      totalArabicValue -= getArabicValue(romNum);
    }
  });
  return totalArabicValue;
}

const setUpCollection = (map) => {
  map.set('M', 1000);
  map.set('CM', 900);
  map.set('D', 500);
  map.set('CD', 400);
  map.set('C', 100);
  map.set('XC', 90);
  map.set('L', 50);
  map.set('XL', 40);
  map.set('X', 10);
  map.set('IX', 9);
  map.set('V', 5);
  map.set('IV', 4);
  map.set('I', 1);
}

export const convertArabicToRoman = (arabicNumeralValue) => {
  const romanNumeralsCollection = new Map();
  setUpCollection(romanNumeralsCollection);
  let convertedRomanNumeral = '';

  romanNumeralsCollection.forEach((romanNumeralValue, romanNumeral) => {
    while (arabicNumeralValue >= romanNumeralValue) {
      convertedRomanNumeral += romanNumeral;
      arabicNumeralValue -= romanNumeralValue;
    }
  });

  return convertedRomanNumeral;
}

export const calculateArabicValue = (currentOperation, currentArabicValue, currentRomNumValue) => {
  if (currentOperation === 'addition') {
    return currentArabicValue + convertRomanToArabic(currentRomNumValue);
  } else if (currentOperation === 'subtraction') {
    return currentArabicValue - convertRomanToArabic(currentRomNumValue);
  } else if (currentOperation === 'multiplication') {
    return currentArabicValue * convertRomanToArabic(currentRomNumValue);
  } else {
    return convertRomanToArabic(currentRomNumValue);
  }
}

/*
  A Roman numeral may only be repeated three times
  Letters V, L, and D may not be repeated 
*/
export const violatesSequenceRule = (romanNumerals, newRomanLetter) => {
  let romNumArray = romanNumerals.split('');
  let lastRomanLetter = romNumArray[romNumArray.length - 1];
  let secondLastRomanLetter = romNumArray[romNumArray.length - 2];
  let thirdLastRomanLetter = romNumArray[romNumArray.length - 3];

  if (['V', 'L', 'D'].includes(newRomanLetter)) {
    return lastRomanLetter === newRomanLetter;
  } else if (lastRomanLetter === newRomanLetter && romanNumerals.length > 2) {
    return lastRomanLetter === secondLastRomanLetter && secondLastRomanLetter === thirdLastRomanLetter
  }
}

/*
  Subtraction only allowed for IV, IX, XL, XC, CD, CM
*/
export const violatesSubtractionRule = (romanNumerals, newRomanLetter) => {
  if (romanNumerals.length < 1) return false;
  const allowedSubtractedNumerals = ['IV', 'IX', 'XL', 'XC', 'CD', 'CM'];
  const currentRomanNumerals = romanNumerals.split('');
  const lastRomanLetter = currentRomanNumerals[currentRomanNumerals.length - 1];
  const secondLastRomanLetter = currentRomanNumerals[currentRomanNumerals.length - 2];
  if (convertRomanToArabic(newRomanLetter) > convertRomanToArabic(lastRomanLetter)) {
    if (allowedSubtractedNumerals.includes(lastRomanLetter + newRomanLetter)) {
      return lastRomanLetter === secondLastRomanLetter;
    } else {
      return true;
    } // 2nd IF
  } else {
    return false;
  } // 1st IF
}

/*
  Verify if subtraction pair is big enough to allow a subsequent letter
*/
export const invalidAfterSubtractionPair = (romanNumerals, newRomanLetter) => {
  const currentRomanNumerals = romanNumerals.split('');
  const finalLetters = currentRomanNumerals[currentRomanNumerals.length - 2] +
    currentRomanNumerals[currentRomanNumerals.length - 1];
  switch (finalLetters) {
    default:
      return false;
    case 'IV':
      return true
    case 'IX':
      return true;
    case 'XL':
      return convertRomanToArabic(newRomanLetter) >= 10;
    case 'XC':
      return convertRomanToArabic(newRomanLetter) >= 10;
    case 'CD':
      return convertRomanToArabic(newRomanLetter) >= 100;
    case 'CM':
      return convertRomanToArabic(newRomanLetter) >= 100;
  }
}