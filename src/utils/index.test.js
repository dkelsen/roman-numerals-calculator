import { 
  convertRomanToArabic, 
  convertArabicToRoman, 
  calculateArabicValue,
  violatesSequenceRule,
  violatesSubtractionRule,
  invalidAfterSubtractionPair,
} from './index';

describe('ConvertRomanToArabic()', () => {
  it('Should convert a Roman numeral to the correct Arabic numeral', () => {
    expect(convertRomanToArabic('LXXIX')).toBe(79);
  });
});

describe('convertArabicToRoman()', () => {
  it('Should convert an Arabic numeral to the correct Roman numeral', () => {
    expect(convertArabicToRoman(845)).toBe('DCCCXLV');
  });
});

describe('calculateArabicValue()', () => {
  it('Should calculate the correct value when lacking an operator', () => {
    expect(calculateArabicValue(null, 20, 'IX')).toBe(9);
  });

  it('Should calculate the correct value for an addition', () => {
    expect(calculateArabicValue('addition', 20, 'IX')).toBe(29);
  });

  it('Should calculate the correct value for a subtraction', () => {
    expect(calculateArabicValue('subtraction', 20, 'IX')).toBe(11);
  });

  it('Should calculate the correct value for a multiplication', () => {
    expect(calculateArabicValue('multiplication', 20, 'IX')).toBe(180);
  });
});

describe('violatesSequenceRule()', () => {
  it('Should return true for a sequence violation of four letters', () => {
    expect(violatesSequenceRule('XXX', 'X')).toBeTruthy();
  });

  it('Should return false for a correct sequence of three letters', () => {
    expect(violatesSequenceRule('II', 'I')).toBeFalsy();
  });

  it('Should return true for a sequence violation of two letters', () => {
    expect(violatesSequenceRule('V', 'V')).toBeTruthy();
  });

  it('Should return false for a correct sequence of two letters', () => {
    expect(violatesSequenceRule('M', 'M')).toBeFalsy();
  });
});

describe('violatesSubtractionRule()', () => {
  it('Should return true for a violation using a viable pair', () => {
    expect(violatesSubtractionRule('XXX', 'C')).toBeTruthy();
  });

  it('Should return true for a non-viable pair', () => {
    expect(violatesSubtractionRule('MD', 'M')).toBeTruthy();
  });

  it('Should return false for a correct order', () => {
    expect(violatesSubtractionRule('MMDXLI', 'X')).toBeFalsy();
  });
});

describe('invalidAfterSubtractionPair()', () => {
  it('Should return true for finding an invalid letter', () => {
    expect(invalidAfterSubtractionPair('CDXCIX', 'I')).toBeTruthy();
  });

  it('Should return false for not finding an invalid letter', () => {
    expect(invalidAfterSubtractionPair('CDXCVI', 'I')).toBeFalsy();
  });
});
