import React from 'react';
import { shallow } from 'enzyme';

import Display from './index';
import { findByTestAttribute } from '../../utils/index';

describe('<Display />', () => {
  let component;

    beforeEach(() => {
      const props = {
        children: 'XVII',
        errorMessage: 'Invalid!'
      }

      component = shallow(<Display {...props} />);
    });

  it('Should render a display wrapper', () => {
    expect(findByTestAttribute(component, 'displayWrapper').length).toBe(1);
  });

  it('Should render one \'div\' tag inside the wrapper', () => {
    expect(findByTestAttribute(component, 'displayWrapper').children().find('div').length).toBe(1);
  });

  it('Should render a Textfit component inside the wrapper', () => {
    expect(findByTestAttribute(component, 'displayWrapper').find('TextFit').length).toBe(1);
  });

  it('Should render errorMessage props', () => {
    expect(findByTestAttribute(component, 'displayWrapper').childAt(1).text()).toEqual('Invalid!');
  });
});