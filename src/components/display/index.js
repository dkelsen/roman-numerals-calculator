import React from  'react';

import { Textfit } from 'react-textfit';
import './styles.scss';

const Display = ({ children, errorMessage }) => {
  return (
    <div className='display' data-test='displayWrapper'>
      <Textfit mode='single' max={48} className='output'>{ children }</Textfit>
      <div className='error-message'>{ errorMessage }</div>
    </div>
  );
}

export default Display;