import React from 'react';
import { shallow } from 'enzyme';

import Button from './index';
import { findByTestAttribute } from '../../utils/index';

describe('<Button />', () => {
  describe('With props', () => {
    let component;
    let mockFunction;

    beforeEach(() => {
      mockFunction = jest.fn();
      const props = {
        columns: 2,
        colored: true,
        children: 'V',
        handleClick: mockFunction
      }

      component = shallow(<Button {...props} />);
    });

    it('Should render a button wrapper', () => {
      expect(findByTestAttribute(component, 'buttonWrapper').length).toBe(1);
    });

    it('Should insert the right \'columns\' class', () => {
      expect(findByTestAttribute(component, 'buttonWrapper').hasClass('two-columns')).toBeTruthy();
    });
  
    it('Should insert a \'colored-background\' class', () => {
      expect(findByTestAttribute(component, 'buttonWrapper').hasClass('colored-background')).toBeTruthy();
    });
  
    it('Should render children props', () => {
      expect(findByTestAttribute(component, 'buttonWrapper').text()).toEqual('V');
    });

    it('Should emit callback function on click event', () => {
      const button = findByTestAttribute(component, 'buttonWrapper');
      button.simulate('click');
      const callback = mockFunction.mock.calls.length;
      expect(callback).toEqual(1);
    });
  }); // End -- With props

  describe('Without props', () => {
    let component;

    beforeEach(() => {
      component = shallow(<Button />);
    });
    
    it('Should not render', () => {
      expect(findByTestAttribute(component, 'buttonWrapper').length).toBe(0);
    });
  }) // End -- Without props
});