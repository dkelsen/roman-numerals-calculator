import React from  'react';

import './styles.scss';

const adjustColumns = columns => {
  switch(columns) {
    case 2:
      return 'two-columns';
    case 3: 
      return 'three-columns';
    default: 
      return null;
  }
}

const isRomanNumeral = value => {
  const romanNumerals = ['I', 'V', 'X', 'L', 'C', 'D', 'M'];
  return romanNumerals.includes(value);
}

const Button = ({ children, columns, colored, handleClick }) => {
  if (!children) {
    return null; 
  } else {
    return (
      <div className={`button 
        ${ columns ? adjustColumns(columns) : "" }
        ${ (!isRomanNumeral(children)) ? 'operator' : "" }
        ${ colored ? 'colored-background' : "" }
      `}
      data-test='buttonWrapper'
      onClick={() => { (children.props) ? handleClick(children.props.operator) : handleClick(children) }}>
        { children }
      </div>
    )
  }
}

export default Button;