import React, { Component } from 'react';

import Button from '../../components/button/index';
import Display from '../../components/display/index';
import {
  convertRomanToArabic,
  convertArabicToRoman,
  calculateArabicValue,
  violatesSequenceRule,
  violatesSubtractionRule,
  invalidAfterSubtractionPair,
} from '../../utils/index';
import './styles.scss';

import EqualSign from '../../images/equalSign';
import PlusSign from '../../images/plusSign';
import MinusSign from '../../images/minusSign';
import MultiplySign from '../../images/multiplySign';

class Calculator extends Component {
  // romNum is short for Roman Numeral
  constructor(props) {
    super(props);
    this.state = {
      currentRomNumValue: '',
      currentArabicValue: 0,
      currentOperation: 'none',
      ongoingCalculations: true,
      errorMessage: '',
    }
  }

  appendRomanLetter = (romanLetter) => {
    let { currentRomNumValue, ongoingCalculations } = this.state;
    if (ongoingCalculations) {
      if (violatesSequenceRule(currentRomNumValue, romanLetter)) {
        return this.setState({
          errorMessage: 'Violation of sequence rule!',
        });
      } else if (violatesSubtractionRule(currentRomNumValue, romanLetter)) {
        return this.setState({
          errorMessage: 'Violation of subtraction rule!',
        });
      } else if (invalidAfterSubtractionPair(currentRomNumValue, romanLetter)) {
        return this.setState({
          errorMessage: 'Invalid!',
        });
      } else {
        let newRomNumValue = currentRomNumValue + romanLetter;
        this.setState({
          currentRomNumValue: newRomNumValue,
          errorMessage: '',
        });
      }
    }
  }

  deleteLastRomanLetter = () => {
    if (this.state.ongoingCalculations) {
      let currentLength = this.state.currentRomNumValue.length;
      let newRomNumValue = this.state.currentRomNumValue.substring(0, currentLength - 1);
      this.setState({
        currentRomNumValue: newRomNumValue,
        errorMessage: '',
      });
    }
  }

  clearCurrentCalculations = () => {
    this.setState({
      currentRomNumValue: '',
      currentArabicValue: 0,
      currentOperation: 'none',
      ongoingCalculations: true,
      errorMessage: '',
    });
  }

  isValidCalculation = (currentOperation, newArabicValue) => {
    if (newArabicValue > 3999) {
      this.setState({
        errorMessage: 'Calculation limit of 3999 exceeded!',
      });
      return false;
    } else if (newArabicValue > 0) {
      return true;
    } else  if (newArabicValue <= 0 && currentOperation !== 'none') {
      this.setState({
        errorMessage: 'Calculations must remain above 0!',
      });
      return false;
    } else {
      this.setState({
        errorMessage: 'Invalid operation!',
      });
      return false;
    }
  }

  executeCalculations = ({
    newOperation,
    currentRomNumValue,
    currentArabicValue,
    currentOperation
  }) => {
    let newArabicValue = calculateArabicValue(currentOperation, currentArabicValue, currentRomNumValue);
    if (this.isValidCalculation(currentOperation, newArabicValue)) {
      return this.setState({
        currentRomNumValue: '',
        currentOperation: newOperation,
        currentArabicValue: newArabicValue,
        errorMessage: '',
      });
    }
  }

  endCalculations = (currentRomNumValue, currentArabicValue, currentOperation) => {
    let newArabicValue = calculateArabicValue(currentOperation, currentArabicValue, currentRomNumValue);
    let newRomanNumerals = convertArabicToRoman(newArabicValue);
    if (this.isValidCalculation(currentOperation, newArabicValue)) {
      return this.setState({
        currentRomNumValue: newRomanNumerals,
        currentOperation: 'none',
        currentArabicValue: 0,
        ongoingCalculations: false,
      });
    }
  }

  executeOperation = (newOperation) => {
    let { currentRomNumValue, currentArabicValue, currentOperation, ongoingCalculations } = this.state;
    if (ongoingCalculations) {
      // Initilialize calculator
      if (currentOperation === 'none') {
        this.setState({
          currentRomNumValue: '',
          currentArabicValue: convertRomanToArabic(currentRomNumValue),
        });
      }
      // -- End
      if (newOperation === 'equals') {
        this.endCalculations(currentRomNumValue, currentArabicValue, currentOperation);
      } else {
        this.executeCalculations({
          newOperation,
          currentRomNumValue,
          currentArabicValue,
          currentOperation,
        });
      }
    }
  }

  render() {
    return (
      <div className='calculator' data-test='calculatorWrapper'>
        <Display errorMessage={this.state.errorMessage} >{this.state.currentRomNumValue}</Display>
        <div className='row' data-test='calculatorRow'>
          <Button handleClick={this.clearCurrentCalculations} columns={3}>CLEAR</Button>
          <Button handleClick={this.executeOperation}><MultiplySign operator={'multiplication'} /></Button>
        </div>
        <div className='row' data-test='calculatorRow'>
          <Button handleClick={this.appendRomanLetter}>C</Button>
          <Button handleClick={this.appendRomanLetter}>D</Button>
          <Button handleClick={this.appendRomanLetter}>M</Button>
          <Button handleClick={this.executeOperation}><MinusSign operator={'subtraction'} /></Button>
        </div>
        <div className='row' data-test='calculatorRow'>
          <Button handleClick={this.appendRomanLetter}>V</Button>
          <Button handleClick={this.appendRomanLetter}>X</Button>
          <Button handleClick={this.appendRomanLetter}>L</Button>
          <Button handleClick={this.executeOperation}><PlusSign operator={'addition'} /></Button>
        </div>
        <div className='row' data-test='calculatorRow'>
          <Button handleClick={this.appendRomanLetter} columns={2}>I</Button>
          <Button handleClick={this.deleteLastRomanLetter}>DEL</Button>
          <Button handleClick={this.executeOperation} colored={true}><EqualSign operator={'equals'} /></Button>
        </div>
      </div>
    )
  }
}

export default Calculator;