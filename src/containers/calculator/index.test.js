import React from 'react';
import { shallow } from 'enzyme';

import Calculator from './index';
import { findByTestAttribute } from '../../utils/index';

describe('<Calculator />', () => {
  let component;

  beforeEach(() => {
    component = shallow(<Calculator />);
  });

  it('Should render a calculator wrapper', () => {
    expect(findByTestAttribute(component, 'calculatorWrapper').length).toBe(1);
  });

  it('Should render four rows', () => {
    expect(findByTestAttribute(component, 'calculatorRow').length).toBe(4);
  });

  it('Should render thirteen Button components', () => {
    expect(findByTestAttribute(component, 'calculatorWrapper').find('Button').length).toBe(13);
  });

  it('Should render a Display component', () => {
    expect(findByTestAttribute(component, 'calculatorWrapper').find('Display').length).toBe(1);
  });
});