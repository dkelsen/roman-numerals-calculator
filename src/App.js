import React from 'react';

import Calculator from './containers/calculator/index';
import './App.scss';

function App() {
  return (
    <div className="App">
      <Calculator />
    </div>
  );
}

export default App;
