import React from 'react';

const MinusSign = () => (
  <svg viewBox="0 0 512 512">
    <path fill="#424242" d="M490.667 234.667H21.333C9.551 234.667 0 244.218 0 256c0 11.782 9.551 21.333 21.333 21.333h469.333c11.782 0 21.333-9.551 21.333-21.333.001-11.782-9.55-21.333-21.332-21.333z"/>
  </svg>
)

export default MinusSign;